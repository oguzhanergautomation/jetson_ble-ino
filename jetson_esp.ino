#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>
#include <esp_task_wdt.h>

#define WDT_TIMEOUT 20
#define SERVICE_UUID        "005e0744-f27c-451e-b1a0-db3e6260176b"
#define CHARACTERISTIC_UUID "5017d567-b47b-4fbc-93a7-b55f89314b4a"

std::string jetson_id = "Jetson-S001";

/**/
BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;

/**/
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint8_t txValue = 0;
String rxValueTemp;
char rxValue[1024];

/**/
uint8_t connect_counter = 0;
String incomingDataBT = "";
uint8_t ok_msg[4] = "ok\n";

/**/
class MyServerCallbacks: public BLEServerCallbacks 
{
  void onConnect(BLEServer* pServer) 
  {
    deviceConnected = true;
  };

  void onDisconnect(BLEServer* pServer) 
  {
    deviceConnected = false;
  }
};
/**/
class MyCallbacks: public BLECharacteristicCallbacks 
{
  void onWrite(BLECharacteristic *pCharacteristic) 
  {
    std::string sendValue = pCharacteristic->getValue();
    if (sendValue.length() > 0) 
    {
     
      if(sendValue[sendValue.length() - 1] == '\n')
      {
        for (int i = 0; i < sendValue.length(); i++)
        {
          Serial.print(sendValue[i]);
        } 
      }
    }
  }
};

void setup() 
{
  Serial.begin(115200);
   
  /* Set Wathcdog */
  esp_task_wdt_init(WDT_TIMEOUT, true); //enable panic so ESP32 restarts
  esp_task_wdt_add(NULL); //add current thread to WDT watch
  
  /* Create the BLE Device */
  BLEDevice::init(jetson_id);

  /* Create the BLE Server */
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  /* Create the BLE Service */
  BLEService *pService = pServer->createService(SERVICE_UUID);

  /* Create a BLE Characteristic */
  pTxCharacteristic = pService->createCharacteristic
  (
     CHARACTERISTIC_UUID,
     BLECharacteristic::PROPERTY_NOTIFY
  );

  pTxCharacteristic->setCallbacks(new MyCallbacks());
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic
  (
    CHARACTERISTIC_UUID,
    BLECharacteristic::PROPERTY_READ |
    BLECharacteristic::PROPERTY_WRITE |
    BLECharacteristic::PROPERTY_NOTIFY
  );
  
  pRxCharacteristic->setCallbacks(new MyCallbacks());

  /* Start the service */
  pService->start();

  // Start advertising
  //  pServer->getAdvertising()->start();
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(0x06);  
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
}

void loop() 
{  
  if(deviceConnected == true)
  {
     esp_task_wdt_reset(); 
  }
  if (Serial.available()) 
  {
    char temp = Serial.read();
    rxValueTemp += temp;
    if (temp == '\n')
    {
      temp = 0;
      strncpy(rxValue, rxValueTemp.c_str(), sizeof(rxValue));
      rxValue[sizeof(rxValue) - 1] = 0;
      pTxCharacteristic->setValue(rxValue);
      pTxCharacteristic->notify();
      rxValueTemp = "";
      memset(rxValue, 0, sizeof(rxValue));
    }
    else if(rxValueTemp == "reset")
    {
      esp_restart();
    }
  }
  /* Disconnecting */
  if (!deviceConnected && oldDeviceConnected) 
  {
    delay(500); // give the bluetooth stack the chance to get things ready
    pServer->startAdvertising(); // restart advertising
    oldDeviceConnected = deviceConnected;
  }
  /* Connecting */
  if (deviceConnected && !oldDeviceConnected) 
  {
    // do stuff here on connecting
    oldDeviceConnected = deviceConnected;
  }
}
